echo "********** Running Flutter Lint - Start **********"
flutter clean
flutter pub get
flutter analyze --pub
echo "********** Running Flutter Lint - End **********"