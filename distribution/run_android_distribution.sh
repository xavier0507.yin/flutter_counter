echo "********** Running Android Dev Distribution for QA - Start **********"
echo "Build type are dev/stage, and you input: $CI_COMMIT_TAG."

cd android
if [[ "$CI_COMMIT_TAG" == *"dev"* ]]; then
  fastlane distributeDevToTesters
elif [[ "$CI_COMMIT_TAG" == *"stage"* ]]; then
  fastlane distributeStageToTesters     # Should change to build stage
elif [[ "$CI_COMMIT_TAG" == *"release"* ]]; then
  fastlane distributeReleaseToTesters
else
  fastlane distributeDevToTesters
fi

echo "********** Running Android Dev Distribution for QA - end **********"